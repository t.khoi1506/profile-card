
$(document).ready(function(){
$('.your-class').slick({
  Infinity: true,
  slidesToShow: 4,
  slidesToScroll: 3,
    arrows: true,
  });
});
let showAll = document.querySelector(".btn-primary");
showAll.addEventListener("click", () => {
  game.cardsArray.forEach((card) => card.classList.add("visible"));
});
var triggerTabList = [].slice.call(document.querySelectorAll('#pills-tab button'))
triggerTabList.forEach(function (triggerEl) {
  var tabTrigger = new bootstrap.Tab(triggerEl)

  triggerEl.addEventListener('click', function (event) {
    event.preventDefault()
    tabTrigger.show()
  })
});
// var triggerEl = document.querySelector('#pills-tab button[data-bs-target="#pills-profile"]')
// bootstrap.Tab.getInstance(triggerEl).show() // Select tab by name

// var triggerFirstTabEl = document.querySelector('#pills-tab li:first-child button')
// bootstrap.Tab.getInstance(triggerFirstTabEl).show() // Select first tab
// var someTabTriggerEl = document.querySelector('#pills-profile')
// var tab = new bootstrap.Tab(someTabTriggerEl)

// tab.show()